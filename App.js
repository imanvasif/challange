import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Main from './screens/Main'
import Detail from './screens/Detail'

  const data = [{
    id: 1,
    Detail: "24 May 2020",
    Type: 'Debt'
    Pay: 'Credit Account'
  },
  {
    id: 2,
    Detail: "24 May 2020",
    Type: 'Debt'
    Pay: 'Credit Account'
  },
  {
    id: 3,
    Detail: "24 May 2020",
    Type: 'Debt'
    Pay: 'Credit Account'
  },
  {
    id: 4,
    Detail: "24 May 2020",
    Type: 'Debt'
    Pay: 'Credit Account'
  },
  {
    id: 5,
    Detail: "24 May 2020",
    Type: 'Debt'
    Pay: 'Credit Account'
  },
  {
    id: 6,
    Detail: "24 May 2020",
    Type: 'Debt'
    Pay: 'Credit Account'
  }
  
  ]

 function App() {
  return (

    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen  name="Main" component={Main} />
        <Stack.Screen  name="Detail" component={Detail} />
      </Stack.Navigator>
    </NavigationContainer>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
});

export default App
