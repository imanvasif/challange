import React from 'react'
import { Text, View, StyleSheet, TextInput, Button } from 'react-native';
import Detail from './Detail'


 class Main extends React.Component {
	render() {
		return (
				<View style={styles.screenSize}>
					<View>
						<Text style={styles.textSize}  >Transactions</Text>
						<TextInput style={styles.inputSize} placeholder='Search'/>
					</View>
					<View>
						<h4 style={{fontWeight: 'bold'}} >Performance</h4>
					</View>
				</View>
		)
	}
}


const styles = StyleSheet.create({
  screenSize: {
    paddingHorizontal: 10,
    paddingVertical: 15
  },
  textSize:{
  	fontSize: 20,
  	fontWeight: 'bold',
    marginVertical: 15
  },
  inputSize:{
  	width: 400,
  	paddingHorizontal: 5,
  	paddingVertical:3,
  	borderWidth: 7,
  	borderColor: 'grey',
  	textAlign: 'center'
  }
});
export default Main